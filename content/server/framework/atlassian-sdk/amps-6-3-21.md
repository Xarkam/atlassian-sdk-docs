---
category: devguide
date: '2018-10-17'
platform: server
product: atlassian-sdk
subcategory: updates
title: AMPS 6.3.21
---
# AMPS 6.3.21

### Release Date: October 17, 2018

### Enhancement / Features:

- [AMPS-1465](https://ecosystem.atlassian.net/browse/AMPS-1465}) - Fix various issues with integration tests 
- [AMPS-1464](https://ecosystem.atlassian.net/browse/AMPS-1464}) - allow cargo-maven2-plugin version to be overwritten
- [AMPS-1463](https://ecosystem.atlassian.net/browse/AMPS-1463}) - CARGO-1337 fix causing problems with tests
- [AMPS-1459](https://ecosystem.atlassian.net/browse/AMPS-1459}) - update latest application versions
- [AMPS-1444](https://ecosystem.atlassian.net/browse/AMPS-1444}) - Updating Tomcat before JIRA tomcat release.

### Bug fixes:

- [AMPS-1460](https://ecosystem.atlassian.net/browse/AMPS-1460}) - When the default HTTP port is in use, a new port is selected but this information is not tracked in AMPS
- [AMPS-1457](https://ecosystem.atlassian.net/browse/AMPS-1457}) - For JIRA 7.12.2 and later upgrade tomcat8 version to 8.5.32
